package gradleproject1;

/**
 *
 * @author troyk
 */
public class Main {
    
    public static class MathLogic {
        
        /**
         * Empty constructor
         */
        public MathLogic() {};
        
        /**
        * Adds two integers together
        *
        * @param a the first integer being added
        * @param b the second integer being added
        * @return the sum of the two integers
        * @since 1.0
        */
        public int add(int a, int b) {
            return a + b;
        }
        
        /**
        * Subtracts one integer from another
        *
        * @param a the integer being subtracted FROM
        * @param b the integer being subtracted from a
        * @return the difference of the two integers
        * @since 1.0
        */
        public int subtract(int a, int b) {
            return a - b;
        }
        
        /**
        * Multiplies two integers together
        *
        * @param a the first integer being multiplied
        * @param b the second integer being multiplied
        * @return the product of the two integers
        * @since 1.0
        */
        public int multiply(int a, int b) {
            return a * b;
        }
        
        /**
        * Divides one integer by another
        *
        * @param num the numerator of the division
        * @param den the denominator of the division
        * @return the quotient of the division
        * @since 1.0
        */
        public int divide(int num, int den) {
            return num/den;
            // needs to throw divide by 0 exception
        }
        
        /**
        * Adds two floats together
        *
        * @param a the first float being added
        * @param b the second float being added
        * @return the sum of the two floats
        * @since 1.0
        */
        public float add(float a, float b) {
            return a + b;
        }
        
        /**
        * Subtracts one float from another
        *
        * @param a the float being subtracted FROM
        * @param b the float a is subtracted BY
        * @return the difference of the two floats
        * @since 1.0
        */
        public float subtract(float a, float b) {
            return a - b;
        }
        
        /**
        * Multiplies two floats together
        *
        * @param a the first float being multiplied
        * @param b the second float being multiplied
        * @return the product of the two floats
        * @since 1.0
        */
        public float multiply(float a, float b) {
            return a * b;
        }
        
        /**
        * Divides one float by another
        *
        * @param num the numerator of the division
        * @param den the denominator of the division
        * @return the quotient of the division
        * @since 1.0
        */
        public float divide(float num, float den) {
            return num/den;
            // Needs to throw divide by 0 exception
        }
        
        /**
        * Adds an int and a float
        *
        * @param a the int being added
        * @param b the float being added
        * @return float that is the sum of a and b
        * @since 1.0
        */
        public float add(int a, float b) {
            return a + b;
        }
        
        /**
        * Subtracts in int from a float
        *
        * @param a the int being subtracted FROM
        * @param b the float being subtracted from a
        * @return float that is the difference of a and b
        * @since 1.0
        */
        public float subtract(int a, float b) {
            return a - b;
        }
        
        /**
        * Multiplies an int and a float
        *
        * @param a the int being multiplied
        * @param b the float being multiplied
        * @return float that is the product of a and b
        * @since 1.0
        */
        public float multiply(int a, float b) {
            return a * b;
        }
        
        /**
        * Divides an int by a float
        *
        * @param num the int that is the numerator of the division
        * @param den the float that is the denominator of the division
        * @return float that is the quotient of the division
        * @since 1.0
        */
        public float divide(int num, float den) {
            return num / den;
            // Needs to throw divide by 0 exception
        }
        
        /**
        * Adds an int and a float
        *
        * @param a the float being added
        * @param b the int being added
        * @return float that is the sum of a and b
        * @since 1.0
        */
        public float add(float a, int b) {
            return a + b;
        }
        
        /**
        * Subtracts an int from a float
        *
        * @param a the float being subtracted FROM
        * @param b the int being subtracted from a
        * @return float that is the differenece of a and b
        * @since 1.0
        */
        public float subtract(float a, int b) {
            return a - b;
        }
        
        /**
        * Adds an int and a float
        *
        * @param a the float being added
        * @param b the int being added
        * @return float that is the sum of a and b
        * @since 1.0
        */
        public float multiply(float a, int b) {
            return a * b;
        }
        
        /**
        * Divides a float by an int
        *
        * @param num the float numerator of the division
        * @param den the int denominator of the division
        * @return float that is the sum of a and b
        * @since 1.0
        */
        public float divide(float num, int den) {
            return num / den;
            //Needs to throw divide by 0 exception
        }
        
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        MathLogic ml_1 = new MathLogic();
        
        /**
         * First integer variable that will be used to test integer methods
         */
        int iA = 15;
        /**
         * Second integer variable that will be used to test integer methods
         */
        int iB = 3;
        
        /**
         * First float variable that will be used to test integer methods
         */
        float fA = 15.1f;
        /**
         * Second float variable that will be used to test integer methods
         */
        float fB = 21.2f;
        
        System.out.println("int iA = " + iA);
        System.out.println("int iB = " + iB);
        System.out.println("float fA = " + fA);
        System.out.println("float fB = " + fB);
        
        System.out.println("int add(int = iA,int = iB): " + ml_1.add(iA,iB));
        System.out.println("int subtract(int = iA,int = iB): " + ml_1.subtract(iA, iB));
        System.out.println("int multiply(int = iA,int = iB): " + ml_1.multiply(iA, iB));
        System.out.println("int divide(int = iA,int = iB): " + ml_1.divide(iA, iB));
        
        System.out.println("float add(float = fA,float = fB): " + ml_1.add(fA, fB));
        System.out.println("float subtract(float = fA,float = fB): " + ml_1.subtract(fA, fB));
        System.out.println("float multiply(float = fA,float = fB): " + ml_1.multiply(fA, fB));
        System.out.println("float divide(float = fA,float = fB): " + ml_1.divide(fA, fB));
        
        System.out.println("float add(float = fA,int = iB): " + ml_1.add(fA, iB));
        System.out.println("float subtract(float = fA,int = iB): " + ml_1.subtract(fA, iB));
        System.out.println("float multiply(float = fA,int = iB): " + ml_1.multiply(fA, iB));
        System.out.println("float divide(float = fA,int = iB): " + ml_1.divide(fA, iB));
        
        System.out.println("float add(int = iA,float = fB): " + ml_1.add(iA, fB));
        System.out.println("float subtract(int = iA,float = fB): " + ml_1.subtract(iA, fB));
        System.out.println("float multiply(int = iA,float = fB): " + ml_1.multiply(iA, fB));
        System.out.println("float divide(int = iA,float = fB): " + ml_1.divide(iA, fB));
        
    }
    
}
